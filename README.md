# Repositorio plantilla: "Compra en tienda"

#!/usr/bin/env pythoh3
import sys
articulos = {}

def anadir(articulo, precio):
    articulos[articulo] = precio

def mostrar():
    for articulo, precio in articulos.items():
        print(articulo + ":", float(precio), "euros", )

def pedir_articulo() -> str:
    articulo = input("articulo: ")
    if articulo in articulos:
        return articulo
    else:
        return pedir_articulo()

def pedir_cantidad() -> float:
    try:
        cantidad = float(input("cantidad: "))
        return cantidad
    except ValueError:
        return pedir_cantidad()

def main():
    sys.argv.pop(0)
    for i in range(0, len(sys.argv), 2):
        try:
            articulo = sys.argv[i]
            precio = float(sys.argv[i + 1])
            anadir(articulo, precio)
            introducido = True
        except IndexError:
            print("Error en argumentos: Hay al menos un articulo sin precio: ", articulo + ".")
            sys.exit()
        except ValueError:
            print("Error en argumentos: ", precio, "no es un precio correcto.")

    try:
        if introducido:
            print("Lista de articulos en la tienda:")
            mostrar()
            print()
            compra = pedir_articulo()
            cantidad = float(pedir_cantidad())
            total = float(articulos[compra]) * cantidad
            print()
            print("Compra total:", cantidad, "de", compra + "a pagar", total)

    except UnboundLocalError:
        print("Error en argumentos: no se han especificado articulos")
        sys.exit()

if __name__ == '__main__':
    main()


